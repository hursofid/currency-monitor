### RUBXEUR exchange rate visualiser tool (Raiffeisenbank Russia online banking) ###

![preview](git-images/f83ec058c23498530b9acc74a7da2231ce097fa8.png)

Prerequisites
=============

- Docker

- Docker compose

Usage
=====

- Clone this repo

- docker-compose build

- docker-compose up -d

- Open http://localhost:8080 in your browser

TO-DO LIST
==========

- Use Python BeautifulSoup instead of parsing html with sed

- .gitlab-ci.yml
