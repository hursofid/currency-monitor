#!/bin/sh
set -e

workdir="/opt/app"
currency_file="$workdir/.curr978.html"
archive_file="/opt/data/curr978_archive.csv"
nonconverted_data="$workdir/.curr978.tmp"
user_agent="User-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Safari/605.1.15"
url_to_parse="https://www.raiffeisen.ru/currency_rates/?active_tab=online"
webroot="/opt/httpdocs"
plots_folder="$webroot/img"

# if running alpine, set correct iconv to use
if [[ $(source /etc/os-release; echo "$ID") == "alpine" ]]; then
  alias iconv="/usr/bin/gnu-iconv"
fi

if [[ ! -f "$archive_file" ]]; then
  echo "Error: $archive_file doesn't exist"
  #touch "$archive_file"
  exit 1
fi

if [[ ! -d "$plots_folder" ]]; then
  echo "Warning: $plots_folder doesn't exist, creating it now..."
  mkdir "$plots_folder"
fi

function GetData() {
  echo "[ $(date '+%d %b %Y %H:%M:%S (%Z)') ] Requesting fresh data from server..."
  curl --silent --connect-timeout 10 --retry 499 --retry-connrefused --retry-delay 5 --header "${user_agent}" \
    "${url_to_parse}" --output "$nonconverted_data"
}

function ConvertData() {
  echo "[ $(date '+%d %b %Y %H:%M:%S (%Z)') ] Converting data..."
  iconv --from-code CP1251 --to-code UTF-8 < "$nonconverted_data" > "$currency_file"
}

function ExtractCurrencyValue() {
grep --after-context 35 'div id="online' "$currency_file" | \
grep --regexp действуют --after-context 1 --regexp Евро | \
sed 's/^\s*//;s/<p>//g;s/<\x2fdiv>//;s/<\x2fp>//;s/<div.*>//;s/--//;/^$/d;s/\\n/;/g;s/Курсы действуют с //;s/ МСК//;s/ /;/' | \
tr '\n' ';' | \
awk -F ';' '{ OFS = FS } {print $2,$1,$3,$4}'
}

function Cleanup() {
  rm "$nonconverted_data" "$currency_file"
}

GenerateGraph() {
period_name="$1"
datafile="$period_name.dat"
plot_image="$period_name.png"
case "$period_name" in
  "last-5y")
    dates="$(for i in $(seq 1825 -1 0); do TZ=Europe/Moscow date --date="$i days ago" "+%d\\.%m\\.%Y"; done)"
    ;;
  "last-3y")
    dates="$(for i in $(seq 1095 -1 0); do TZ=Europe/Moscow date --date="$i days ago" "+%d\\.%m\\.%Y"; done)"
    ;;
  "last-1y")
    dates="$(for i in $(seq 364 -1 0); do TZ=Europe/Moscow date --date="$i days ago" "+%d\\.%m\\.%Y"; done)"
    ;;
  "last-30d")
    dates="$(for i in $(seq 29 -1 0); do TZ=Europe/Moscow date --date="$i days ago" "+%d\\.%m\\.%Y"; done)"
    ;;
  "last-7d")
    dates="$(for i in $(seq 6 -1 0); do TZ=Europe/Moscow date --date="$i days ago" "+%d\\.%m\\.%Y"; done)"
    ;;
  "last-24h")
    dates="$(for i in $(seq 23 -1 0); do TZ=Europe/Moscow date --date="$i hours ago" "+%d\\.%m\\.%Y;%H"; done)"
    ;;
  *)
    echo "Invalid period name: $period_name"
    exit 1
    ;;
esac

: > "$datafile"
for date in $dates; do
  grep "$date" "$archive_file" >> "$datafile" || true
done
sed -i "1 s/^/# X Y\\n/" "$datafile"
# Cleanup dataset from invalid values
sed -i "/оформ/d" "$datafile"
# Final formatting for gnuplot
sed -i "s/;Евро;/ /" "$datafile"
echo "[ $(date '+%d %b %Y %H:%M:%S (%Z)') ] Generating $plot_image graph..."
case "$period_name" in
  "last-5y")
    gnuplot -e "set xdata time; set timefmt \"%d.%m.%Y;%H:%M\"; set format x \"%b\"; set term png nocrop enhanced size 640,480 medium; set output \"$plots_folder/$plot_image\"; plot \"$datafile\" using 1:2 with lines"
    ;;
  "last-3y")
    gnuplot -e "set xdata time; set timefmt \"%d.%m.%Y;%H:%M\"; set format x \"%b\"; set term png nocrop enhanced size 640,480 medium; set output \"$plots_folder/$plot_image\"; plot \"$datafile\" using 1:2 with lines"
    ;;
  "last-1y")
    gnuplot -e "set xdata time; set timefmt \"%d.%m.%Y;%H:%M\"; set format x \"%b\"; set term png nocrop enhanced size 640,480 medium; set output \"$plots_folder/$plot_image\"; plot \"$datafile\" using 1:2 with lines"
    ;;
  "last-30d"|"last-7d")
    gnuplot -e "set xdata time; set timefmt \"%d.%m.%Y;%H:%M\"; set format x \"%d %b\"; set term png nocrop enhanced size 640,480 medium; set output \"$plots_folder/$plot_image\"; plot \"$datafile\" using 1:2 with lines"
    ;;
  "last-24h")
    gnuplot -e "set xdata time; set timefmt \"%d.%m.%Y;%H:%M\"; set format x \"%H:%M\"; set term png nocrop enhanced size 640,480 medium; set output \"$plots_folder/$plot_image\"; plot \"$datafile\" using 1:2 with lines"
    ;;
  *)
    echo "Invalid period name: $period_name"
    exit 1
    ;;
esac
}

GetData
ConvertData
if [ "$(ExtractCurrencyValue)" != "$(tail --lines 1 "$archive_file")" ]; then
  #ExtractCurrencyValue | tee --append "$archive_file" # this is used to show latest value
  echo "[ $(date '+%d %b %Y %H:%M:%S (%Z)') ] Data has changed on server, writing to archive..."
  ExtractCurrencyValue >> "$archive_file"
  echo "[ $(date '+%d %b %Y %H:%M:%S (%Z)') ] Generating graphs..."
  GenerateGraph last-5y
  GenerateGraph last-3y
  GenerateGraph last-1y
  GenerateGraph last-30d
  GenerateGraph last-7d
  GenerateGraph last-24h
  else
    echo "[ $(date '+%d %b %Y %H:%M:%S (%Z)') ] No changes, sleeping..."
fi
Cleanup

echo "[ $(date '+%d %b %Y %H:%M:%S (%Z)') ] Workflow of $0 complete..."

exit 0
